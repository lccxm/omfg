
import java.util.ArrayList;
import java.util.Arrays;

public class Human {

    private int x;
    private int y;

    public Human(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void moveTo(String location, Map map) {
        if (location.equalsIgnoreCase("up")) {
            if (map.findHuman(getX(), getY() - 1) == null && getY() != 0) {
                setY(getY() - 1);
            }
        } else if (location.equalsIgnoreCase("down")) {
            if (map.findHuman(getX(), getY() + 1) == null && getY() != map.getMaxY()) {
                setY(getY() + 1);
            }
        } else if (location.equalsIgnoreCase("left")) {
            if (map.findHuman(getX() - 1, getY()) == null && getX() != 0) {
                setX(getX() - 1);
            }
        } else if (location.equalsIgnoreCase("right")) {
            if (map.findHuman(getX() + 1, getY()) == null && getX() != map.getMaxX()) {
                setX(getX() + 1);
            }
        }
    }

    public ArrayList<Human> getNearBlocks(Map m) {

        int i = 0;
        ArrayList<Human> nearEnemies = new ArrayList<>();
        if (m.findHuman(getX() + 1, getY()) != null) {
            nearEnemies.add(m.findHuman(getX() + 1, getY()));

        }
        if (m.findHuman(getX() - 1, getY()) != null) {
            nearEnemies.add(m.findHuman(getX() - 1, getY()));

        }
        if (m.findHuman(getX(), getY() - 1) != null) {
            nearEnemies.add(m.findHuman(getX(), getY() - 1));

        }
        if (m.findHuman(getX(), getY() + 1) != null) {
            nearEnemies.add(m.findHuman(getX(), getY() + 1));

        }

        return nearEnemies;
    }

    public void attack(Map map, Human entity) {
        ArrayList<Human> humans = getNearBlocks(map);
        for (Human h : humans) {
            if (humans.contains(entity)) {
                map.remove(entity, map);
            }
        }
    }

    public int sort() {
        int rand = (int) (Math.random() * 101);
        return rand;
    }

    public void randomMove(Map map) {

        int location = (int) (Math.random() * 5);
        if (location == 0) {
            location++;
        }
        switch (location) {
            case 1:
                moveTo("up", map);
                break;
            case 2:
                moveTo("down", map);
                break;
            case 3:
                moveTo("left", map);
                break;
            case 4:
                moveTo("right", map);
                break;
        }
    }
}
