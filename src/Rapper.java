public class Rapper extends Human {

    private boolean curse;
    private int roundsCursed;

    public Rapper(int x, int y) {
        super(x, y);
        curse = false;
    }

    public boolean getCurse() {
        return curse;
    }

    public void swap() {
        curse = !curse;
        if (curse) {
            roundsCursed = 5;
        }
    }

    public int getRoundsCursed() {
        return roundsCursed;
    }

    public void decreaseCurse() {
        roundsCursed--;
    }


}
